﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;
using System.Net;
using System.IO;
using System.Drawing;
using System.Data.Entity.Validation;

namespace EntityProject
{
    public class SmallContext
    {
        public static string GoogleHImageImagePath = "thimage/";
        public static string GoogleBaseURLPath = "https://storage.cloud.google.com/" + GoogleHImageImagePath;

        public static void SaveImages(string externalID, List<string> localFilePaths)
        {
            int saved = 0;
            int count = 0;
            try
            {
                using (var db = new SmallDBEntities())
                {
                    foreach (string filePath in localFilePaths)
                    {
                        try
                        {
                            bool proceed = false;
                            string source = GoogleBaseURLPath + filePath;
                            if (db.NormalImages.FirstOrDefault(n => n.sourceURL == source) == null)
                            {
                                proceed = true;
                            }

                            if (proceed)
                            {
                                NormalImage image = new NormalImage();
                                image.externalID = externalID;
                                image.sourceURL = filePath;
                                image.createdAt = DateTime.Now;
                                image.updatedAt = DateTime.Now;
                                image.sourceURL = "" + filePath;
                                image.sampleURL = "";
                                image.previewURL = "";
                                image.deleted = false;

                                Image bitmap = Image.FromFile(filePath);
                                image.sourceHeight = bitmap.Height;
                                image.sourceWidth = bitmap.Width;

                                db.NormalImages.Add(image);
                                count++;
                            }

                            if (count % 50 == 0)
                            {
                                saved += db.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                    }

                    saved += db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }

            Console.WriteLine(localFilePaths.Count + " / " + saved + " files saved to database.");
        }


        public static void SaveHImage(string externalID, string filePath, string cloudURL)
        {
            int saved = 0;
            try
            {
                using (var db = new SmallDBEntities())
                {
                    try
                    {
                        bool proceed = false;
                        NormalImage check = db.NormalImages.FirstOrDefault(n => n.sourceURL == cloudURL);
                        proceed = check == null;

                        if (proceed)
                        {
                            NormalImage image = new NormalImage();
                            image.externalID = externalID;
                            image.createdAt = DateTime.Now;
                            image.updatedAt = DateTime.Now;
                            image.sourceURL = cloudURL;
                            image.sampleURL = "";
                            image.previewURL = "";
                            image.deleted = false;

                            Image bitmap = Image.FromFile(filePath);
                            image.sourceHeight = bitmap.Height;
                            image.sourceWidth = bitmap.Width;

                            db.NormalImages.Add(image);
                            saved += db.SaveChanges();

                            if(saved > 0)
                            {
                                Console.WriteLine("Saved " + cloudURL + " (" + externalID + ")");
                            }
                            else
                            {
                                Console.WriteLine("Did not save " + cloudURL + " (" + externalID + ")");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Image already exists in database. Skipping...");
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
        }
    }
}
