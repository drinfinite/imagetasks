﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ImageTasks
{
    public class Greyscale
    {

        public static void MakeTraining()
        {
            Console.Write("Directory? (empty for default): ");

            string dir = Console.ReadLine();

            if (dir == "")
            {
                dir = "D:/ai/img/source/";
            }

            if (dir.Contains("source"))
            {

                Console.Write("Extension? (default *): ");

                string ext = Console.ReadLine();

                if (ext == "")
                {
                    ext = "*";
                }

                ProcessDirectory(dir, ext);
                Console.WriteLine("Done.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Directory did not contain 'source'. Quitting.");
                Console.ReadLine();
            }
        }

        static void ProcessDirectory(string directory, string ext)
        {
            int counter = 0;
            Directory.CreateDirectory(directory.Replace("/source/", "/destination/"));
            foreach (string file in Directory.GetFiles(directory, "*." + ext, SearchOption.AllDirectories))
            {
                string properName = file.Replace("\\", "/");

                if (!File.Exists(properName.Replace("/source/", "/destination/")))
                {
                    if (!file.Contains(".db") && !file.Contains(".ink") && !file.Contains(".lnk"))
                    {
                        counter++;
                        Bitmap image = (Bitmap)Image.FromFile(properName);
                        ProcessFile(image, properName);
                        image.Dispose();
                    }
                }

                if (counter % 100 == 0)
                {
                    GC.Collect();
                }
            }
        }

        static void ProcessFile(Bitmap image, string file)
        {
            Bitmap normalized = General.Normalize(image, file);
            if (normalized != null)
            {
                Bitmap greyscale = (Bitmap)GreyScale(normalized, file);

                if (greyscale != null)
                {
                    Bitmap combined = General.Combine(greyscale, normalized);

                    if (combined != null)
                    {
                        combined.Save(file.Replace("/source/", "/destination/"), ImageFormat.Jpeg);

                        Console.WriteLine("[SUCCESS] " + file);
                    }
                    else
                    {
                        Console.WriteLine("[FAILURE] " + file + " on combine.");
                    }
                }
                else
                {
                    Console.WriteLine("[FAILURE] " + file + " on greyscale.");
                }
            }
            else
            {
                Console.WriteLine("[FAILURE] " + file + " on normalize.");
            }
        }

        static Image GreyScale(Bitmap image, string file)
        {
            Bitmap greyscale = new Bitmap(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    Color pixelColor = image.GetPixel(x, y);
                    //  0.3 · r + 0.59 · g + 0.11 · b
                    int grey = (int)(pixelColor.R * 0.3 + pixelColor.G * 0.59 + pixelColor.B * 0.11);
                    //int grey = (int)(pixelColor.R * 0.1 + pixelColor.G * 0.85 + pixelColor.B * 0.05);
                    greyscale.SetPixel(x, y, Color.FromArgb(pixelColor.A, grey, grey, grey));
                }
            }
            greyscale.Save(file.Replace("/source/", "/A/"));
            return greyscale;
        }


        /// <summary>
        /// Resize an image keeping its aspect ratio (cropping may occur).
        /// </summary>
        /// <param name="source"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Bitmap ResizeImageKeepAspectRatio(Image source, int width, int height, string file)
        {
            Bitmap result = null;

            try
            {
                if (source.Width != width || source.Height != height)
                {
                    // Resize image
                    float sourceRatio = (float)source.Width / source.Height;

                    using (var target = new Bitmap(width, height))
                    {
                        using (var g = Graphics.FromImage(target))
                        {
                            g.CompositingQuality = CompositingQuality.HighQuality;
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.SmoothingMode = SmoothingMode.HighQuality;

                            // Scaling
                            float scaling;
                            float scalingY = (float)source.Height / height;
                            float scalingX = (float)source.Width / width;
                            if (scalingX < scalingY)
                            {
                                scaling = scalingX;
                            }
                            else
                            {
                                scaling = scalingY;
                            }

                            int newWidth = (int)(source.Width / scaling);
                            int newHeight = (int)(source.Height / scaling);

                            // Correct float to int rounding
                            if (newWidth < width) newWidth = width;
                            if (newHeight < height) newHeight = height;

                            // See if image needs to be cropped
                            int shiftX = 0;
                            int shiftY = 0;

                            if (newWidth > width)
                            {
                                shiftX = (newWidth - width) / 2;
                            }

                            if (newHeight > height)
                            {
                                shiftY = (newHeight - height) / 2;
                            }

                            //Remove transparency
                            g.Clear(Color.White);
                            g.CompositingMode = CompositingMode.SourceOver;

                            // Draw image
                            if (General.resize)
                            {
                                g.DrawImage(source, -shiftX, -shiftY, newWidth, newHeight);
                            }
                            else
                            {
                                g.DrawImage(source, -shiftX, -shiftY, source.Width, source.Height);
                            }
                        }

                        result = (Bitmap)target.Clone();
                        //result.Save(file.Replace("/source/", "/B/"));
                    }
                }
                else
                {
                    // Image size matched the given size
                    result = (Bitmap)source.Clone();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                result = null;
            }

            return result;
        }
    }
}
