﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Security.Cryptography;

namespace ImageTasks
{
    public class General
    {
        //everything becomes this height and width
        public static int resizeTo = 512;
        public static bool resize = false;


        public static Bitmap Normalize(Bitmap image, string file)
        {
            Bitmap normalized = PadImage(image);
            normalized = Resize(normalized, resizeTo, resizeTo, file);
            image.Dispose();
            return normalized;
        }


        public static Bitmap PadImage(Bitmap originalImage)
        {
            int largestDimension = Math.Max(originalImage.Height, originalImage.Width);
            Size squareSize = new Size(largestDimension, largestDimension);
            Bitmap squareImage = new Bitmap(squareSize.Width, squareSize.Height);
            using (Graphics graphics = Graphics.FromImage(squareImage))
            {
                graphics.FillRectangle(Brushes.Black, 0, 0, squareSize.Width, squareSize.Height);
                int widthDifference = (largestDimension - originalImage.Width) / 2;
                int heightDifference = (largestDimension - originalImage.Height) / 2;
                graphics.DrawImage(originalImage, widthDifference, heightDifference, originalImage.Width, originalImage.Height);
            }
            originalImage.Dispose();
            return squareImage;
        }

        public static string GetMD5Hash(byte[] bytes)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(bytes);
                foreach (byte b in hash)
                {
                    stringBuilder.Append(b.ToString("x2"));
                }
            }
            return stringBuilder.ToString();
        }

        public static string GetMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static bool VerifyMD5Hash(string input, string hash)
        {
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            return comparer.Compare(GetMD5Hash(input), hash) == 0;
        }

        /// <summary>
        /// Turns all files inside a directory into files named after the MD5 of their content
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static int RenameDirectory(string path)
        {
            int changed = 0;

            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories))
                {
                    string newName = General.GetMD5Hash(File.ReadAllBytes(file));
                    string newPath = file.Replace(Path.GetFileNameWithoutExtension(file), newName);
                    if (!File.Exists(newPath))
                    {
                        File.Move(file, newPath);
                        Console.WriteLine("  Moved [" + file + "] -> [" + newPath + "]");
                        changed++;
                    }
                    else
                    {
                        if (newPath != file)
                        {
                            File.Delete(file);
                            Console.WriteLine("Deleting already processed " + file);
                        }
                        else
                        {
                            Console.WriteLine("Skipping " + file + " because it already exists.");
                        }
                    }
                }
            }
            return changed;
        }

        public static Bitmap Resize(Image source, int width, int height, string file)
        {
            Bitmap resized = new Bitmap(source, new Size(width, height));
            //resized.Save(file.Replace("/source/", "/B/"));
            source.Dispose();
            return resized;
        }


        public static Bitmap Combine(Bitmap left, Bitmap right)
        {
            if (left.Width != resizeTo && right.Width != resizeTo)
            {
                if (resize)
                {
                    throw new Exception("Image not " + resizeTo + "x" + resizeTo + " on resize.");
                }
            }
            Bitmap combined = new Bitmap(left.Width + right.Width, left.Height);

            using (Graphics graphics = Graphics.FromImage(combined))
            {
                graphics.DrawImage(left, 0, 0, resizeTo, resizeTo);
                graphics.DrawImage(right, resizeTo, 0, resizeTo, resizeTo);
            }

            left.Dispose();
            right.Dispose();

            return combined;
        }

    }
}
