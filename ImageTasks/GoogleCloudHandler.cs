﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Google.Api;
using Google.Cloud.Storage.V1;
using Google.Apis.Auth.OAuth2;

namespace ImageTasks
{
    public class GoogleCloudHandler
    {
        public string UploadFile(string bucketName, string localPath, string objectName = null)
        {
            GoogleCredential credential = GoogleCredential.FromFile("D:/Projects/ImageTasks/ImageTasks/ImageTasks/Credentials/mfp.json");
            StorageClient storage = StorageClient.Create(credential);
            string cloudPath = "";

            using (FileStream fileStream = File.OpenRead(localPath))
            {
                objectName = objectName ?? Path.GetFileName(localPath);
                Google.Apis.Storage.v1.Data.Object uploadedObject = storage.UploadObject(bucketName, objectName, null, fileStream);
                Console.WriteLine("    Uploaded " + uploadedObject.Name);
                cloudPath = "https://storage.cloud.google.com/" + bucketName + "/" + uploadedObject.Name;
            }

            return cloudPath;
        }
    }
}
