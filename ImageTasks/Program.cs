﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ImageTasks
{
    class Program
    {

        static void Main(string[] args)
        {
            while (true)
            {
                string choice = "";

                while (!choice.Contains("q"))
                {
                    Console.Write("(G)reyscale, (s)obel, (r)ename, (d)ownload, or (u)pload file training images?: ");
                    choice = Console.ReadLine().ToLower();

                    if (choice.Contains("s"))
                    {
                        Sobel.MakeSobel();
                    }
                    else if (choice.Contains("g"))
                    {
                        Greyscale.MakeTraining();
                    }
                    else if (choice.Contains("r"))
                    {
                        Console.Write("Enter the directory to autorename all files within: ");
                        string filePath = Console.ReadLine();
                        int changed = General.RenameDirectory(filePath);
                        Console.WriteLine("Renamed " + changed + " files to their MD5'd contents.");
                    }
                    else if (choice.Contains("d"))
                    {

                    }
                    else if (choice.Contains("u"))
                    {
                        Uploader uploader = new Uploader();
                        Console.Write("File path to upload?: ");
                        string filePath = Console.ReadLine();
                        Console.WriteLine();

                        uploader.UploadFile(filePath);
                    }

                    Console.WriteLine("Done all. Press enter to do another task.");
                    Console.ReadLine();
                }
            }
        }
    }
}
