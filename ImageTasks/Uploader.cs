﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ImageTasks
{
    public class Uploader
    {
        public void UploadFile(string filePath)
        {
            GoogleCloudHandler handler = new GoogleCloudHandler();
            string basePath = "D:/AI/txtdata";
            string recordsFile = "D:/AI/txtdata/uploaded.txt";
            List<string> alreadyProcessed = new List<string>();

            if (File.Exists(recordsFile))
            {
                using (StreamReader sr = File.OpenText(recordsFile))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        alreadyProcessed.Add(s);
                    }
                }
            }

            if (!Directory.Exists(basePath))
            {
                Directory.CreateDirectory(basePath);
            }

            if (File.Exists(filePath))
            {
                handler.UploadFile("thimage", filePath);
                Console.WriteLine("Uploaded image.");
            }
            else if(Directory.Exists(filePath))
            {
                Console.Write("Also save in database? [Y/n]: ");
                bool save = false;

                if (Console.ReadLine().ToLower().Contains("y"))
                {
                    save = true;
                }

                List<string> filePaths = new List<string>();
                foreach(string file in Directory.GetFiles(filePath, "*.jpg", SearchOption.AllDirectories))
                {
                    filePaths.Add(file.Replace("\\", "/"));
                }
                foreach (string file in Directory.GetFiles(filePath, "*.png", SearchOption.AllDirectories))
                {
                    filePaths.Add(file.Replace("\\", "/"));
                }

                foreach (string file in filePaths)
                {
                    if (alreadyProcessed.Contains(file))
                    {
                        Console.WriteLine("Already processed " + file + ", skipping...");
                    }
                    else
                    {
                        string cloudURL = handler.UploadFile("thimage", file);

                        if (save)
                        {
                            DirectoryInfo directoryInfo = Directory.GetParent(file);
                            EntityProject.SmallContext.SaveHImage(directoryInfo.Name, file, cloudURL);
                        }

                        using (StreamWriter sw = File.AppendText(recordsFile))
                        {
                            sw.WriteLine(file);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Not a file or directory.");
            }

            Console.WriteLine("Uploader done.");
        }
    }
}
