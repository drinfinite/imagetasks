﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.IO;
using System.Drawing;
using Microsoft.WindowsAPICodePack.Shell;
using System.Diagnostics;

namespace Terracana.Data
{
    public class BlobHandler
    {
        /// <summary>
        /// Uploads a file to a blob container for cheaper storage.
        /// </summary>
        /// <param name="fileNameAndPath">The file name and path of the local item to upload.</param>
        /// <param name="deleteLocallyAfterUploading">Deletes the local copy if and only if it uploads successfully.</param>
        /// <returns>The blob URI</returns>
        public static string UploadCustomerFile(string fileNameAndPath, bool deleteLocallyAfterUploading, string containerName, bool generatePreview)
        {
            if (generatePreview)
            {
                GeneratePreview(fileNameAndPath);
            }
            string connectionString = Environment.GetEnvironmentVariable("AzureConnectionString");
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = Environment.GetEnvironmentVariable("AzureConnectionString", EnvironmentVariableTarget.User);
            }
            string message = "";

            if (!string.IsNullOrEmpty(connectionString))
            {
                //Create a BlobServiceClient object which will be used to create a container client
                BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);

                BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName);

                BlobClient blobClient = containerClient.GetBlobClient(Path.GetFileName(fileNameAndPath));

                Debug.WriteLine("Uploading to Blob storage as blob: " + blobClient.Uri);
                message = blobClient.Uri.ToString();

                // Open the file and upload its data
                FileStream uploadFileStream = File.OpenRead(fileNameAndPath);
                blobClient.Upload(uploadFileStream, true);
                uploadFileStream.Close();

                if (deleteLocallyAfterUploading)
                {
                    File.Delete(fileNameAndPath);
                }
            }
            else
            {
                Debug.WriteLine("Could not get blob credentials. Missing required variables or there is insufficient access on this process to access the required credentials.");
            }

            return message;
        }

        /// <summary>
        /// Uploads a blob compliant file 
        /// </summary>
        /// <param name="fileNameAndPath"></param>
        /// <param name="deleteLocallyAfterUploading"></param>
        /// <returns></returns>
        public async Task<string> UploadCustomerFileAsync(string fileNameAndPath, bool deleteLocallyAfterUploading, string containerName, bool generatePreview)
        {
            if (generatePreview)
            {
                GeneratePreview(fileNameAndPath);
            }

            string connectionString = Environment.GetEnvironmentVariable("AzureConnectionString");
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = Environment.GetEnvironmentVariable("AzureConnectionString", EnvironmentVariableTarget.User);
            }
            string message = "";

            if (!string.IsNullOrEmpty(connectionString))
            {
                // Create a BlobServiceClient object which will be used to create a container client
                BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);

                // Create the container and return a container client object
                BlobContainerClient containerClient = await blobServiceClient.CreateBlobContainerAsync(containerName);

                // Get a reference to a blob
                BlobClient blobClient = containerClient.GetBlobClient(fileNameAndPath);

                System.Diagnostics.Debug.WriteLine("Uploading to Blob storage as blob: " + blobClient.Uri);
                message = blobClient.Uri.ToString();

                // Open the file and upload its data
                using (FileStream uploadFileStream = File.OpenRead(fileNameAndPath))
                {
                    await blobClient.UploadAsync(uploadFileStream, true);
                }

                if (deleteLocallyAfterUploading)
                {
                    File.Delete(fileNameAndPath);
                }
            }
            else
            {
                Debug.WriteLine("BlobHandler.cs Could not get blob credentials. The environment is missing the required variables or there is insufficient access on this process to access the required credentials");
            }

            return message;
        }

        public static string GetPreviewPath(string filePath)
        {
            if (filePath.Contains("_Preview.") || !filePath.Contains("."))
            {
                return filePath;
            }

            int place = filePath.LastIndexOf(".");

            filePath = filePath.Remove(place, 1).Insert(place, "_Preview.");

            //if it's a video, the shellfile saves it as a bmp that can be served as jpg
            return filePath.Replace(".avi", ".jpg").Replace(".mov", ".jpg").Replace(".mp4", ".jpg");
        }

        public static string GetNonpreviewPath(string filePath)
        {
            return filePath.Replace("_Preview.", ".");
        }

        private static void GeneratePreview(string path)
        {
            try
            {
                if (path.EndsWith(".jpg") || path.EndsWith(".png") || path.EndsWith(".jpeg") || path.EndsWith(".bmp"))
                {
                    using (Bitmap bitmap = new Bitmap(path))
                    {
                        int reduceFactor = 1;

                        if (bitmap.Width > 8192 || bitmap.Height > 8192)
                        {
                            reduceFactor = 8;
                        }
                        else if (bitmap.Width > 4096 || bitmap.Height > 4096)
                        {
                            reduceFactor = 4;
                        }
                        else if (bitmap.Width > 2048 || bitmap.Height > 2048)
                        {
                            reduceFactor = 3;
                        }
                        else if (bitmap.Width > 1360 || bitmap.Height > 1360)
                        {
                            reduceFactor = 2;
                        }

                        Image preview = bitmap.GetThumbnailImage(bitmap.Width / reduceFactor, bitmap.Height / reduceFactor, null, IntPtr.Zero);
                        preview.Save(GetPreviewPath(path));
                        UploadCustomerFile(GetPreviewPath(path), true, "==========", false);
                    }
                }
                else if (path.EndsWith(".avi") || path.EndsWith(".mp4") || path.EndsWith(".mov"))
                {
                    ShellFile shellFile = ShellFile.FromFilePath(path);
                    Bitmap preview = shellFile.Thumbnail.ExtraLargeBitmap;
                    preview.Save(GetPreviewPath(path));
                    UploadCustomerFile(GetPreviewPath(path), true, "=======", false);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("MakePreviewImage(" + path + ")");
            }
        }
    }
}
